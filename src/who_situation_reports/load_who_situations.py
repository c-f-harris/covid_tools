#
#   DEPRACATED WARNING: For case numbers, you should instead use the .csv data
#                       from the JHU CSSE git repository with all case numbers
#                       by region.
#
#   This script loads and parses all of the situation report PDFs from the WHO.
#

import requests
from datetime import datetime
import PyPDF2
import os
import matplotlib.pyplot as plt
import numpy as np


#-----------------------------------------------------------
# Constants
#-----------------------------------------------------------
month_to_int = {
    'January': 1,
    'February': 2,
    'March': 3,
    'April': 4,
    'May': 5,
    'June': 6,
    'July': 7,
    'August': 8,
    'September': 9,
    'October': 10,
    'November': 11,
    'December': 12,
}

WHO_SITUATION_URL = "https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports"

INITIAL_DATE = datetime( 2020, month_to_int['January'], 21 )   # 21 January 2020, 1st situation report


#-----------------------------------------------------------
# WHO site sensitive parameters
#-----------------------------------------------------------
SITUATION_PDF_URL_SERVER_PREFIX = "/docs/default-source/coronaviruse/situation-reports/"
SITUATION_PDF_URL_PREFIX = "https://www.who.int/%s" % SITUATION_PDF_URL_SERVER_PREFIX

START_DELIM = "PageContent_C006_Col01"
END_DELIM = "PageContent_C006_Col02"

VIEW = 9

def first_int( string ):
    string = string.split(' ')
    for s in string:
        try:
            z = int(s)
            break
        except ValueError:
            z = None
    return z


class Internet(object):
    def get_request(self, url):
        return requests.get( url )


class WHO_Situation(object):
    """
    PDF loader / parser
    """
    def __init__(self, pdf_url, date):

        self.pdf_content = None
        self.url = pdf_url
        self.date = date

        self.time_index = ( self.date - INITIAL_DATE ).days

        #------------------------------------------------
        # Parameters to parse
        #------------------------------------------------
        self.param = {

            'global_cases': None,

            #--------------------------------------------
            # Cases by country
            #--------------------------------------------
            'us_cases': None,
            'uk_cases': None,
            'italy_cases': None,
            'germany_cases': None,
            'china_cases': None,
            'japan_cases': None,
            'spain_cases': None,
            'republic_of_korea_cases': None,
        }

    def load(self):
        if self.url is None:
            print("Error: Aiming to load null WHO Situation URL\n")
        response = Internet().get_request(self.url)
        if response.ok:
            self.pdf_content = response.content
        else:
            print("Error: Unable to load %s" % self.url)

    def parse(self):
        """
        Parse PDF
        """
        if self.pdf_content is None:
            print("Error: Parsing null situation\n")
            return

        #---------------------------------------------------
        # Temporarily write to file for parser
        #---------------------------------------------------
        tmp_file = open('tmp_file.pdf', 'wb')
        tmp_file.write(self.pdf_content)
        tmp_file.close()

        tmp_file = open('tmp_file.pdf', 'rb')
        pdf_parser = PyPDF2.PdfFileReader(tmp_file)

        txt = ''
        for i in range(pdf_parser.getNumPages()):
            txt += pdf_parser.getPage(i).extractText()
        txt = txt.replace('\n', '').split('United States')

        idx = 1
        while idx < len(txt):
            if ' Center' in txt[idx][:10]:
                idx+=1
            else:
                cnt = first_int( txt[idx][:30] )
                self.param['us_cases'] = cnt
                break

        #---------------------------------------------------
        # Cleanup
        #---------------------------------------------------
        tmp_file.close()
        os.remove('tmp_file.pdf')


class WHO_Site_Parser(object):
    """
    Parser for the webiste that lists all the WHO COVID Situations
    """
    def __init__(self):
        self.url = WHO_SITUATION_URL

    def get_covid_situations(self):
        #---------------------------------------------------
        # Load COVID situation webpage content (html file)
        #---------------------------------------------------
        response = Internet().get_request( self.url )
        situations = []
        if response.ok:
            content = response.content.decode('utf-8')

            content = content.split( START_DELIM )[1]
            content = content.split( END_DELIM )[0]
            content = content.split( SITUATION_PDF_URL_SERVER_PREFIX )
            #---------------------------------------------------
            # Remove the first non-situation and reverse into
            # chronological order
            #---------------------------------------------------
            del content[0]
            content.reverse()

            #---------------------------------------------------
            # Watch out for repeated URLs
            # (must be a programming mistake)
            #---------------------------------------------------
            visited_urls = set()
            for sit in content:
                if '.pdf' not in sit or '?' not in sit:
                    continue
                sit_pdf_name = sit.split('">')[0]
                if sit_pdf_name not in visited_urls:
                    visited_urls.add( sit_pdf_name )

                    sit_url = "%s/%s" % ( SITUATION_PDF_URL_PREFIX, sit_pdf_name )

                    d = sit.split("/>")[2].split("<")[0].split(' ')
                    sit_date = datetime( int(d[2]), month_to_int[d[1]], int(d[0]) )

                    situations.append( WHO_Situation( pdf_url=sit_url, date=sit_date ) )
        else:
            print( "Error: Unable to load %s" % self.url )

        return situations



if __name__ == "__main__":

    site_parser = WHO_Site_Parser()
    situations = site_parser.get_covid_situations()

    # Validity check
    for i in range(len(situations)):
        if situations[i].time_index != i:
            print("\n\nError: Time index mismatch. See if WHO COVID site changed HTML or if they skipped a day.\n\n")

    # Load PDFs
    for sit in situations:
        print("Loading %s...\n" % sit.url)
        sit.load()

    for sit in situations:
        print("Parsing %s WHO COVID Situation PDF...\n" % sit.date)
        sit.parse()

    # Plot US cases
    last_cnt = None
    cnt = []

    for i in range(len(situations)):
        if situations[i].param['us_cases'] is not None:
            last_cnt = situations[i].param['us_cases']
        elif situations[i].param['us_cases'] is None and last_cnt is not None:
            situations[i].param['us_cases'] = last_cnt
        else:
            situations[i].param['us_cases'] = 0

        cnt.append(situations[i].param['us_cases'])

    plt.plot(np.linspace(0, len(cnt), len(cnt)), cnt)
    plt.ylabel('U.S. Cases')
    plt.xlabel('Days since 21 January 2020')
    plt.title('COVID Cases in U.S.')
    plt.show()
