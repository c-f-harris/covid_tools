# optimize.py
# Functions for optimization.

#----------------------------------------------------------------
# Imports
#----------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares  # Non-linear least squares

#----------------------------------------------------------------
# Non-linear least squares
#----------------------------------------------------------------
def fit_curve(t_train, y_train, func, x0):
    """
    Use non-linear least squares to fit a curve to training data

    :param t_train: samples
    :param y_train: targets
    :param func: residual function
    :return: coefficients for curve fit (a local optimum)
    """
    res = least_squares(func, x0, args=(t_train, y_train), jac="3-point")
    return res.x, res.cost

#----------------------------------------------------------------
# Common residual functions
#----------------------------------------------------------------
def exponential_residual(x, t, y):
    return x[0] * np.exp(x[1] * t) - y

def linear_residual(x, t, y):
    return x[0] + x[1] * t - y

#----------------------------------------------------------------
# Example
#----------------------------------------------------------------
if __name__ == "__main__":

    N = 100
    t = np.linspace(0, 20, N)
    r = np.random.uniform(0, 1, N)
    y = np.exp(t)*r

    x0 = [1.0]
    x, cost = fit_curve(t, y, exponential_residual, x0)
    yhat = [np.exp(x[0]*t[i]) for i in range(len(t))]

    print("X    = %.5f" % x)
    print("COST = %.5f" % cost)

    plt.plot(t, y, c="b")
    plt.plot(t, yhat, c="r")
    plt.show()

#
