# timeseries.py
# Functions and objects for time series, signals, and systems

#----------------------------------------------------------------
# Imports
#----------------------------------------------------------------
import datetime

#----------------------------------------------------------------
# Generic Time Series Class
#----------------------------------------------------------------
class Time_Series:

    def __init__(self, dates=None):
        """
        Initialize time series with global day vector
        dates: list of datetime objects in sorted order
        """
        #--------------------------------------------------------
        # Calculate datetimes
        #--------------------------------------------------------
        self.datetimes = dates
        self.day_strings = [day.strftime("%m-%d-%Y") for day in self.datetimes]
        self.day_zero = None
        self.time = []

        if dates is not None:
            self.day_zero = self.datetimes[0]
            #--------------------------------------------------------
            # Calculate "number of day" vector from first day in data
            #--------------------------------------------------------
            self.time = [(d - self.day_zero).days for d in self.datetimes]

    @staticmethod
    def append(lst, new_pt):
        """
        Add new point to time series vector.
        Handle NULL point by adding previous valid point if exists, else zero.
        """
        if new_pt is None:
            if len(lst) == 0:
                lst.append(0)
            else:
                lst.append(lst[-1])
        else:
            lst.append(new_pt)

    def time_series_str(self, lst):
        return " // ".join([str((t, p)) for (t,p) in zip(self.time, lst)])


#----------------------------------------------------------------
# First order derivative
#----------------------------------------------------------------
def derivative_first_order(x):
    x_prime = [x[i] - x[i-1] for i in range(1, len(x))]
    return x_prime
