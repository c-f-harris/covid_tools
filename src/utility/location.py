# location.py
# Interface for the geopy library

#----------------------------------------------------------------
# Imports
#----------------------------------------------------------------
from geopy.geocoders import Nominatim


#----------------------------------------------------------------
# Constants
#----------------------------------------------------------------

# Google maps URL (API key is required)
GOOGLE_MAPS_LAT_LONG_REQUEST = "https://maps.googleapis.com/maps/api/geocode/"

# geopy Nominatim instance
geolocator = Nominatim()


#----------------------------------------------------------------
# Coordinate class
#----------------------------------------------------------------
class Coordinates(object):

    def __init__(self, lat=None, long=None):
        self.latitude = lat
        self.longitude = long

    def __str__(self):
        return str(self.latitude) + "," + str(self.longitude)

    @staticmethod
    def get_city_coordinates(location_string):
        """
        Return Coordinates object given "city, region"
        """
        loc = geolocator.geocode(location_string)
        return Coordinates(lat=loc.latitude, long=loc.longitude)
