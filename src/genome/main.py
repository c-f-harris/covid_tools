#
# Pass assembly sample ID, e.g. MT188339
#
import sys

from genome_parser import load_genome


if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("\nNo Assembly ID, using MT188339 as default.\n")
        id = "MT188339"
    else:
        id = sys.argv[1]

    sample_info, genome = load_genome(id)
    
    print("\n")
    print(genome, "\n")
    print("Sample Assembly ID: %s\n" % id)
    print("Base Pairs in Genome = %d\n" % len(genome))
    print("Sampled on %s\n" % sample_info[0])
