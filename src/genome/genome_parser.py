
import re

DATA_DIRECTORY = "../../data/genome"
SAMPLES_DIRECTORY = "%s/samples" % DATA_DIRECTORY
METADATA_PATH = "%s/metadata.csv" % DATA_DIRECTORY
BASES = set(['c','a','t','g'])


def load_genome(id):
    """
    :param id: ID of COVID-19 sample
    :type id: string
    :return: (sample info, genome)
    """
    # Metadata (csv format)
    infile = open(METADATA_PATH, "r")
    metadata = [z.split(",") for z in infile.read().split("\n") if len(z) > 0]
    metadata_columns = metadata[0]
    metadata = { z[0]:z[1:] for z in metadata[1:] }
    infile.close()

    # This sample's metadata:
    sample_info = [z for z in metadata[id] if len(z.strip()) > 0]

    # Parse the genome data:
    infile = open(SAMPLES_DIRECTORY + "/" + id + ".txt", "r")
    genome = infile.read().split("ORIGIN")
    genome = genome[1]
    infile.close()

    # Regular expression to strip all but the bases
    genome = re.sub(r'[^.cagt]', "", genome)

    return (sample_info, genome)
