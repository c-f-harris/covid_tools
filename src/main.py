# main.py
# COVID tools main menu

#----------------------------------------------------------------
# Imports
#----------------------------------------------------------------
import argparse
import sys
sys.path += ["covid_cases", "utility"]

from covid_cases.cache import cache_update_covid_data, cache_load_covid_data
from covid_cases.covid_analysis import plot_regions, get_highest_regions

#----------------------------------------------------------------
# Relative Path to Cache
#----------------------------------------------------------------
CACHEPATH="covid_cases/cache/cache.txt"

#----------------------------------------------------------------
# Arguments
#----------------------------------------------------------------
def parse_arguments():

    parser = argparse.ArgumentParser()

    # Load data to cache
    parser.add_argument("--load_cache", "-l", help="Load data to cache", action="store_true")

    # Region options
    parser.add_argument("--united_states", "-us", help="Select regions in the United States", action="store_true")
    parser.add_argument("--file", "-f", help="Select regions listed in regions.txt", action="store_true")

    # Category
    parser.add_argument("--confirmed", "-c", help="Select number of confirmed cases", action="store_true")
    parser.add_argument("--deaths", "-d", help="Select number of deaths", action="store_true")
    parser.add_argument("--recovered", "-r", help="Select number of recoveries", action="store_true")

    # Actions
    parser.add_argument("--plot", "-p", help="Plot time series for region in BLUE", action="store_true")
    parser.add_argument("--derivative", "-der", help="Plot derivative (1st order) of data, if --plot option", action="store_true")

    # Extrapolation
    parser.add_argument("--extrapolate", "-ex", help="Extrapolate --extrap_days in the future (RED curve)", action="store_true")
    parser.add_argument("--extrap_days", "-ex_day", help="Number of days to extrapolate, if --extrapolate, default 3 days", type=int, default=3 )
    parser.add_argument("--leave_out", help="Leave out days from extrapolation for testing", type=int, default=0)
    parser.add_argument("--curve", help="Fit data to name of curve. Options: 'exponential', 'line', ", default="exponential")
    args = parser.parse_args()

    #---------------------------------------------------------------
    # Default Options
    #---------------------------------------------------------------
    # Category
    if not args.confirmed and not args.deaths and not args.recovered:
        args.confirmed = True
    # Action:
    if not args.plot:
        args.plot = True

    return args


def parse_regions_from_file(path="regions.txt"):
    infile = open(path, "r")
    regions = infile.read().splitlines()
    infile.close()
    while "" in regions: regions.remove("")
    i = 0
    while i < len(regions):
        if regions[i].replace(" ", "")[0] == "#":
            del regions[i]
        else:
            i+=1
    return regions


def get_regions(args, category, data):

    regions = []
    for r in data.keys():
        if args.united_states:
            regions = [r for r in data.keys() if r[-2:] == "US"]
        elif args.file:
            regions = parse_regions_from_file()
        else:
            regions = data.keys()

    # Always sort by largest
    regions = get_highest_regions(data, category, region_subset=regions, derivative=args.derivative)
    return regions


def main():

    args = parse_arguments()

    #---------------------------------------------------------------
    # Load data
    #---------------------------------------------------------------
    if args.load_cache:
        cache_update_covid_data(cachepath=CACHEPATH)
    data = cache_load_covid_data(cachepath=CACHEPATH)

    #---------------------------------------------------------------
    # Category
    #---------------------------------------------------------------
    if args.confirmed:
        category = "confirmed"
    elif args.deaths:
        category = "deaths"
    else:
        category = "recovered"

    #---------------------------------------------------------------
    # Regions
    #---------------------------------------------------------------
    regions = get_regions(args, category, data)

    if args.plot:
        plot_regions(regions, data, category, args)


if __name__ == "__main__":
    main()
