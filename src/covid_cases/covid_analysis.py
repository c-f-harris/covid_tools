#   covid_analysis.py
#   Time series analysis of COVID data

#-----------------------------------------------------------------
# Imports
#-----------------------------------------------------------------
# Python
import sys
import os
import numpy as np
import datetime
import matplotlib.pyplot as plt

# Local imports
sys.path.append("../utility")
from location           import  Coordinates
from timeseries         import  Time_Series, derivative_first_order
from optimize           import  fit_curve, exponential_residual, linear_residual


def get_highest_regions(data, category, region_subset=None, derivative=False):
    """
    Return regions sorted in order of category
    """
    if region_subset is None:
        region_subset = data.keys()

    if "confirmed" == category:
        most = [[region, data[region].confirmed[-1]] for region in region_subset]
    elif "deaths" == category:
        most = [[region, data[region].deaths[-1]] for region in region_subset]
    else:
        most = [[region, data[region].recovered[-1]] for region in region_subset]

    most.sort(key=lambda x: x[1], reverse=True)
    regions = [z[0] for z in most]
    return regions

def get_signal(region_data, category):
    if "confirmed" == category: signal = region_data.confirmed
    elif "deaths" == category: signal = region_data.deaths
    else: signal = region_data.recovered
    return signal

#------------------------------------------------------------
# Plotting
#------------------------------------------------------------

def plot_regions(regions, data, category, args):

    derivative = args.derivative
    extrap_days = args.extrap_days
    leave_out = args.leave_out

    for region in regions:

        rd = data[region]
        time = rd.time
        signal = get_signal(rd, category)

        title = region.upper() + " " + category.upper()
        if derivative == True:
            title += " Derivative"
            time = time[1:]
            signal = derivative_first_order(signal)

        # Extrapolate by fitting the data to an exponential curve
        if args.extrapolate:
            extrapolated_time, extrapolated_confirmed = extrapolate(
                time,
                signal,
                tf=extrap_days,
                leave_out=leave_out,
                curve=args.curve
            )

            plt.plot(extrapolated_time, extrapolated_confirmed, c="red")

        plt.title(title)
        plt.plot(time, signal, c="blue")
        plt.scatter(time, signal, c="blue")
        plt.xlabel("Days Since March 10, 2020")
        plt.ylabel(category)
        plt.show()

#------------------------------------------------------------
# Extrapolation
#------------------------------------------------------------

def extrapolate(time, signal, tf=3, x0=[0.01,0.01], leave_out=0, curve="exponetial"):

    if curve == "line": residual = linear_residual
    elif curve == "exponential": residual = exponential_residual
    else:
        print("\nError: Trying to extrapolate with unknown residual curve %s.\n" % curve)
        curve = "exponential"

    final_index = len(time) - leave_out
    x, cost = fit_curve(np.array(time[:final_index]), np.array(signal[:final_index]), residual, x0)

    extrapolated_time = np.linspace(0, time[-1] + tf, 1000)
    extrapolated_confirmed = [residual(x, t, 0) for t in extrapolated_time]
    return extrapolated_time, extrapolated_confirmed

#
