# case_parser.py
# Loads JHU CSSE case time series data.

#----------------------------------------------------------------
# Imports
#----------------------------------------------------------------

# Python
import sys
import numpy as np
import pandas as pd
import os
import csv
import re
import datetime
import matplotlib.pyplot as plt

# Local imports
sys.path.append("../utility")
from location       import      Coordinates
from timeseries     import      Time_Series, derivative_first_order
from optimize       import      fit_curve, exponential_residual, linear_residual

#----------------------------------------------------------------
# Constants
#----------------------------------------------------------------
JHU_PATH = "../../data/COVID-19"
if False == os.path.isdir(JHU_PATH):
    JHU_PATH = "../data/COVID-19"

JHU_DAILY_CASE_PATH = "%s/csse_covid_19_data/csse_covid_19_daily_reports/" % JHU_PATH
CACHE_DELIM = "/~"

#----------------------------------------------------------------
# COVID Case Time Series Class
#----------------------------------------------------------------
class COVID_Time_Series(Time_Series):

    def __init__(self, dates=None, province=None, region=None, coordinates=None):
        super(COVID_Time_Series, self).__init__(dates)
        self.region = region
        self.province = province
        self.confirmed = []
        self.deaths = []
        self.recovered = []
        self.coordinates = coordinates

    def set_coordinates(self):
        self.coordinates = \
            Coordinates.get_city_coordinates(self.province + "," + self.region)

    def add_confirmed(self, confirmed):
        Time_Series.append(self.confirmed, confirmed)

    def add_deaths(self, deaths):
        Time_Series.append(self.deaths, deaths)

    def add_recovered(self, recovered):
        Time_Series.append(self.recovered, recovered)

    @staticmethod
    def get_key(province, region):
        """
        Return key for location->COVID_Time_Series dictionary
        """
        return province + ", " + region if province != "" else region

    def key(self):
        return COVID_Time_Series.get_key(self.province, self.region)

    def cache_to_string(self):
        """
        Convert self to string
        """
        s = ""
        s += ",".join(self.day_strings) + CACHE_DELIM
        s += self.region + CACHE_DELIM
        s += self.province + CACHE_DELIM
        s += ",".join([str(z) for z in self.confirmed]) + CACHE_DELIM
        s += ",".join([str(z) for z in self.deaths]) + CACHE_DELIM
        s += ",".join([str(z) for z in self.recovered]) + CACHE_DELIM
        s += str(self.coordinates) # end char here
        return s

    @staticmethod
    def cache_load(cache_string):
        """
        Load COVID_Time_Series from string
        """
        elements = cache_string.split(CACHE_DELIM)

        # Datetimes
        date_strings = elements[0].split(",")
        datetimes = [datetime.datetime(z[2], z[0], z[1])\
                        for z in [[int(x) for x in day.split("-")]\
                        for day in date_strings]]

        # Region, province
        region = elements[1]
        province = elements[2]

        # Time series
        confirmed = [float(z) for z in elements[3].split(",")]
        deaths = [float(z) for z in elements[4].split(",")]
        recovered = [float(z) for z in elements[5].split(",")]

        # Coordinates
        coordinates = elements[6]
        if "None" not in coordinates:
            x = [float(z) for z in coordinates.split(",")]
            coordinates = Coordinates(lat=x[0], long=x[1])
        else:
            coordinates = None

        time_series = COVID_Time_Series(dates=datetimes, province=province, region=region, coordinates=coordinates)
        time_series.confirmed = confirmed
        time_series.deaths = deaths
        time_series.recovered = recovered
        return time_series


    def __str__(self):
        s = "\n\nCONFIRMED:\n" + self.time_series_str(self.confirmed)
        s += "\n\nDEATHS\n" + self.time_series_str(self.deaths)
        s += "\n\nRECOVERED\n" + self.time_series_str(self.recovered)
        return s


#----------------------------------------------------------------
# COVID parser functions
#----------------------------------------------------------------

def _load_case_files():
    """
    Load file string for each daily report in the JHU daily report directory
    Return: dictionary mapping filename -> report string
    """

    #------------------------------------------------------------
    # Get file paths in the JHU daily report directory
    #------------------------------------------------------------
    file_paths = [x for x in os.listdir(JHU_DAILY_CASE_PATH) if ".csv" in x]
    relative_paths = [JHU_DAILY_CASE_PATH + f for f in file_paths]

    #------------------------------------------------------------
    # Load all file contents into a dictionary indexed by file
    #------------------------------------------------------------
    reports = {}
    for i in range(len(relative_paths)):
        infile = open(relative_paths[i], "r")
        content = infile.read()
        infile.close()
        reports[file_paths[i]] = content
    return reports


def _load_covid_daily_reports():
    """
    Return: Dictionary of Pandas DataFrames with daily report csv as key.
    """
    reports = _load_case_files()
    for r in reports:
        #--------------------------------------------------------
        # Use CSV reader to split comma separated string,
        # ignoring the quote character "
        #--------------------------------------------------------
        values = reports[r].split("\n")
        while '' in values: values.remove('')
        values = [[x for x in list(csv.reader([row], delimiter=',', quotechar='"'))[0]] for row in values]

        #--------------------------------------------------------
        # Create DataFrame
        # Note: to avoid differences in '/' and '_' over time,
        # only use letters and numbers in column names.
        #--------------------------------------------------------
        df = pd.DataFrame(values[1:])
        df.columns = [re.sub("[^a-z0-9]+", "", v, flags=re.IGNORECASE) for v in values[0]]

        #--------------------------------------------------------
        # Let Region be ProvinceState, CountryRegion if
        # ProvinceState is not None, else CountryRegion
        #--------------------------------------------------------
        df["Region"] = df[["ProvinceState", "CountryRegion"]].apply\
            (lambda x: ', '.join(x) if x[0] != '' else x[1], axis=1)
        df = df.set_index("Region")
        reports[r] = df

    return reports


def load_covid_cases(t0="03-10-2020"):
    """
    Get COVID_Time_Series data beginning at t0 (by default March 10, 2020)

    Return: Dictionary indexed by ProvinceState containing instance of
            COVID_Time_Series.
    """
    t0 = [int(z) for z in t0.split("-")]
    t0 = datetime.datetime(t0[2], t0[0], t0[1])  # datetime(year, month, day)

    #------------------------------------------------------------
    # Load JHU daily reports from files
    #------------------------------------------------------------
    reports = _load_covid_daily_reports()

    #------------------------------------------------------------
    # Sum up all regions in a new dataframe.
    #------------------------------------------------------------
    df = {} # dictionary of DataFrames containing numeric data
    regions = []  # set of all seen regions

    for r in reports: # For each daily report
        #--------------------------------------------------------
        # Let the new dictionary be indexed by the date (not .csv filename)
        # Let the new DataFrame for this date be indexed by CountryRegion
        # (more precision can be added when needed)
        #--------------------------------------------------------
        date = r.split(".")[0]

        # Skip if this is before day 0 argument
        date = [int(z) for z in date.split("-")]
        date = datetime.datetime(date[2], date[0], date[1]) # datetime(year, month, day)
        if (date - t0).days < 0:
            continue

        df[date] = reports[r][["ProvinceState", "CountryRegion", "Confirmed", "Deaths", "Recovered"]]

        #--------------------------------------------------------
        # Sum up all cases, deaths, and recovered for the CountryRegion
        #--------------------------------------------------------
        numeric_columns = ["Confirmed", "Deaths", "Recovered"]
        for col in numeric_columns:
            df[date][col] = pd.to_numeric(df[date][col])
        df[date] = df[date].groupby(["ProvinceState", "CountryRegion"]).sum()

        #--------------------------------------------------------
        # Update region list
        #--------------------------------------------------------
        for region in df[date].index:
            if region not in regions:
                regions.append(region)

    #------------------------------------------------------------
    # Construct time series data for each region
    #------------------------------------------------------------
    dates = [key for key in df.keys()]
    dates.sort()

    visited = set()
    time_series = {}
    for r in regions:  # for each (ProvinceState, CountryRegion) pair

        province, region = r
        key = COVID_Time_Series.get_key(province, region)

        time_series[key] = COVID_Time_Series(dates=dates, province=province, region=region)
        if key not in visited:
            visited.add(key)
        else:
            print("\n\n\nERROR: MULTIPLE KEYS APPEAR IN DATA: %s\n\n\n" % key)
            return None

        for dt in dates: # for each datetime in sorted order

            confirmed, deaths, recovered = None, None, None
            if r in df[dt].index:
                local_df = df[dt].loc[province, region]
                confirmed = local_df["Confirmed"]
                deaths = local_df["Deaths"]
                recovered = local_df["Recovered"]

            time_series[key].add_confirmed(confirmed)
            time_series[key].add_deaths(deaths)
            time_series[key].add_recovered(recovered)

    return time_series


if __name__ == "__main__":

    from cache import cache_update_covid_data

    #------------------------------------------------------------
    # Load cases
    #------------------------------------------------------------
    data = load_covid_cases(t0="03-10-2020")    # Load data since March 10, 2020

    #------------------------------------------------------------
    # Update cache now that we loaded the data
    #------------------------------------------------------------
    cache_update_covid_data(data=data)

    #------------------------------------------------------------
    # Get AZ data and load coordinates
    #------------------------------------------------------------
    # data["Arizona, US"].set_coordinates()
    # az = data["Arizona, US"]
    # print(str(az.coordinates))

    #------------------------------------------------------------
    # Pretend like it was a few days ago to test model's accuracy
    # (fit curve with data up until leave_out days ago, but plot all data)
    #------------------------------------------------------------
    leave_out = 0

    #------------------------------------------------------------
    # Select type of curve to fit data to
    #------------------------------------------------------------
    residual = exponential_residual
    # residual = linear_residual

    #------------------------------------------------------------
    # Scatter cases for each state and extrapolate for the
    # next 5 days
    #------------------------------------------------------------
    keys = ["Arizona, US", "Kansas, US", "California, US", "New York, US", "Massachusetts, US"]
    # keys = data.keys()  # all states
    for region in keys:

        print(region)

        if region[-2:] == "US":

            rd = data[region] # region data

            #----------------------------------------------------
            # Fit a function to the data
            #----------------------------------------------------
            x0 = [1.0, 1.0]
            final_index = len(rd.time) - leave_out
            x, cost = fit_curve(np.array(rd.time[:final_index]), np.array(rd.confirmed[:final_index]), residual, x0)

            tf = 5  # n days ahead of now (assuming JHU data is pulled up)
            extrapolated_time = np.linspace(0, rd.time[-1] + tf, 1000)
            extrapolated_confirmed = [residual(x, t, 0) for t in extrapolated_time]

            #----------------------------------------------------
            # Plot data and extrapolation
            #----------------------------------------------------
            rd = data[region]
            plt.title(region + " Confirmed Cases (dots) and %d Day Extrapolation" % tf)
            plt.scatter(rd.time, rd.confirmed, c="red")
            plt.plot(extrapolated_time, extrapolated_confirmed, c="blue")
            plt.show()

            plt.title(region + " Derivative of Confirmed Cases")
            plt.plot(rd.time[1:], derivative_first_order(rd.confirmed), c='r')
            plt.show()

#
