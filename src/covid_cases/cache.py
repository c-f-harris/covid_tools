#   cache.py
#   Store JHU COVID data after loading

#----------------------------------------------------------------
# Imports
#----------------------------------------------------------------
# Python imports
import os
import sys
from datetime import datetime
import matplotlib.pyplot as plt

# Local imports
from jhu_case_parser import load_covid_cases, COVID_Time_Series, CACHE_DELIM


#----------------------------------------------------------------
# Constants
#----------------------------------------------------------------
DEFAULT_CACHE = "cache/cache.txt"


def cache_load_covid_data(cachepath=DEFAULT_CACHE):
    """
    Load data from cache
    """
    if False == os.path.isfile(cachepath):
        print("\nError: Invalid path to cache.\n")
        return None
    #--------------------------------------------------------
    # Load strings from file
    #--------------------------------------------------------
    data = {}
    infile = open(cachepath, "r")
    content = infile.read().splitlines()
    infile.close()
    #--------------------------------------------------------
    # Check time stamp and print warning if more than 1 day
    # old
    #--------------------------------------------------------
    z = [int(x) for x in content[0].split("-")]
    date = datetime(z[2], z[0], z[1])

    if (datetime.now() - date).days >= 1:
        print("\n---Cached data is more than 1 day old---\n\nRecommend pulling up JHU data and re-caching\n\n")

    #--------------------------------------------------------
    # Parse each region
    #--------------------------------------------------------
    for rd in content[1:]:
        if len(rd) > 0:
            time_series = COVID_Time_Series.cache_load(rd)
            data[time_series.key()] = time_series

    return data


def cache_update_covid_data(t0="03-10-2020", data=None, cachepath=DEFAULT_CACHE):
    """
    Load COVID data and store it in the cache.
    :param t0: initial date (string format, March 10, 2020 for default)
    :param data: if data is None, load it here
    """

    if data is None:
        data = load_covid_cases(t0=t0)

    outfile = open(cachepath, "w")
    #--------------------------------------------------------
    # Write timestamp on first line (just need day precision
    # for this)
    #--------------------------------------------------------
    outfile.write(datetime.now().strftime("%m-%d-%Y") + "\n")
    #--------------------------------------------------------
    # Write each region's data to file (string format)
    #--------------------------------------------------------
    for region in data:
        outfile.write(data[region].cache_to_string() + "\n")
    outfile.close()
    print("\nWrote %d regions to %s\n" % (len(data), cachepath))


if __name__ == "__main__":

    cache_update_covid_data()
    data = cache_load_covid_data()

    for region in data:
        rd = data[region]
        plt.title(region + " confirmed (blue), deaths (red), recovered (green)")
        plt.plot(rd.time, rd.confirmed, c="blue")
        plt.plot(rd.time, rd.deaths, c="red")
        plt.plot(rd.time, rd.recovered, c="green")
        plt.show()
