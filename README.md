# OBJECTIVES #

1) To train a model that predicts the coefficient of spread (probability of person to person transmission) of the Coronavirus using sentiment analysis of Tweets that contain the string "corona".

2) To use genetic algorithms to simulate the spread of the Coronavirus using the predicted coefficient of spread.


## COVID-19 GENOME DATA ##

* Source: https://www.ncbi.nlm.nih.gov/labs/virus/vssi/#/virus?SeqType_s=Nucleotide&VirusLineage_ss=Severe%20acute%20respiratory%20syndrome-related%20coronavirus,%20taxid:694009

* NOTE: Recently, many more DNA sequences have been posted on this site.


## COVID-19 DATA (submodule added to data folder) ##

* Git repository maintained by Johns Hopkins University

* Daily situation reports with case numbers are posted by JHU CSSE in CSV format

* Also, the WHO case reports are posted in CSV format


## COVID-19 Tweet Sentiments ##

* Source: https://ieee-dataport.org/open-access/corona-virus-covid-19-tweets-dataset

* An LSTM was trained by Rabindra Lamsal (JNU, New Delhi) to perform sentiment analysis

* Every tweet is given a sentiment score between -1 and 1

------------------------------------------------------------------------------------------
